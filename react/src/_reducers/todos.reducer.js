import { todoConstants } from '../_constants';

export function todos(state = {}, action) {
  switch (action.type) {
    case todoConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case todoConstants.GETALL_SUCCESS:
      return {
        items: action.todos
      };
    case todoConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    case todoConstants.CREATE_REQUEST:
      return { registering: true };
    case todoConstants.CREATE_SUCCESS:
      return {};
    case todoConstants.CREATE_FAILURE:
      return {};
    case todoConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(todo =>
          todo._id === action.id
            ? { ...todo, deleting: true }
            : todo
        )
      };
    case todoConstants.DELETE_SUCCESS:
      return {
        items: state.items.filter(todo => todo._id !== action.id)
      };
    case todoConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(todo => {
          if (todo._id === action.id) {
            const { deleting, ...todoCopy } = todo;
            return { ...todoCopy, deleteError: action.error };
          }

          return todo;
        })
      };
    default:
      return state
  }
}