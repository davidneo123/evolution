import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';

class UsersPage extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    render() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <button className="btn btn-success">
                    <strong><Link to="/Register">Agregar usuario</Link></strong>
                </button>
                <h3>Usuarios registrados:</h3>
                {users.loading && <em>Cargando usuarios...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <ul>
                        {users.items.map((user, index) =>
                            <li key={user._id}>
                                <Link to={{
                                    pathname: "/Register",
                                    state: user}}
                                >
                                    {user.username }{user.admin?'. Administrador':null}
                                </Link>
                                {
                                    user.deleting ? <em> - Eliminando...</em>
                                    : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                    : <span> - <a onClick={this.handleDeleteUser(user._id)}>  (-) Eliminar </a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedUsersPage = connect(mapState, actionCreators)(UsersPage);
export { connectedUsersPage as UsersPage };