import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { todoActions, alertActions } from '../../_actions';

class TodosPage extends React.PureComponent {
    componentDidMount() {
        this.props.getTodos()
    }

    handleDeleteTodo(id) {
        return (e) => this.props.deleteTodo(id);
    }


    render() {
        const { todos } = this.props;
       
        return (
            <div className="col-md-6 col-md-offset-3">
                 <button className="btn btn-success"><Link to= "/todo">Agregar Tarea</Link></button>
                <h3>Tareas por realizar:</h3>
                {todos.loading && <em>Cargando tareas...</em>}
                {todos.error && <span className="text-danger">ERROR: {todos.error}</span>}
                {todos.items &&
                    <ul>
                        {todos.items.map((todo, index) =>
                            <li key={todo._id}>
                                <Link to={{
                                pathname: "/todo",
                                state: todo}}>
                                    <strong>{todo.name + ' Prioridad: ' + todo.priority}</strong>
                                </Link>
                                {
                                    todo.deleting ? <em> - Eliminando...</em>
                                    : todo.deleteError ? <span className="text-danger"> - ERROR: {todo.deleteError}</span>
                                    : <span> - <a onClick={this.handleDeleteTodo(todo._id)}>  (-) Eliminar </a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

function mapState(state) {
    const { todos } = state;
    return { todos };
}

const actionCreators = {
    getTodos: todoActions.getAll,
    deleteTodo: todoActions.delete,
    messenger: alertActions.success

}

const connectedTodosPage = connect(mapState, actionCreators)(TodosPage);
export { connectedTodosPage as TodosPage };