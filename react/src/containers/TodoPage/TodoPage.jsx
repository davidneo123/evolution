import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { todoActions } from '../../_actions';

class TodoPage extends React.Component {
    constructor(props) {
        super(props);
        var date=props.location.state?props.location.state.expire_at.substr(0,10):'2020-01-01';
        this.state = {
            todo: {
                _id:props.location.state&&props.location.state._id||'',
                name: props.location.state&&props.location.state.name||'',
                priority:props.location.state&&props.location.state.priority||0,
                expire_at:date
            },
            submitted: false,
            update: props.location.state&&props.location.state._id?true:false
        };

        console.log(this.state)
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { todo } = this.state;
        let valueM=value;
        name=='priority'?valueM=JSON.parse(valueM):null;
        this.setState({
            todo: {
                ...todo,
                [name]: valueM
            }
        })
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { todo, update } = this.state;
        let newTodo = todo;
        newTodo.expire_at=new Date(newTodo.expire_at).toISOString();
        console.log(newTodo)
        if (todo.name && todo.expire_at && todo.priority ) {
            if(update){
                this.props.create(newTodo,false);
            }
            else{
                delete newTodo._id;
                this.props.create(newTodo,true);
            }
        }
    }

    render() {
        const { registering  } = this.props;
        const { todo, update, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>{update?'Actualizar tarea':'Crear tarea'}</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !todo.name ? ' has-error' : '')}>
                        <label htmlFor="todoname">Nombre tarea</label>
                        <input type="text" className="form-control" name="name" value={todo.name} onChange={this.handleChange} />
                        {submitted && !todo.name &&
                            <div className="help-block">Nombre de la tarea es requerido</div>
                        }
                    </div>
                   <div className={'form-group' + (submitted && todo.priority==0 ? ' has-error' : '')}>
                        <label htmlFor="priority">Prioridad</label>
                        <input type="number" className="form-control" name="priority" value={todo.priority} onChange={this.handleChange} />
                        {submitted && todo.priority==0 &&
                            <div className="help-block">Prioridad es requerido</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !todo.expire_at ? ' has-error' : '')}>
                        <label htmlFor="expire_at">Fecha Vencimiento</label>
                        <input type="date" className="form-control" name="expire_at" value={todo.expire_at} onChange={this.handleChange} />
                        {submitted && todo.priority==0 &&
                            <div className="help-block">Fecha de Vencimiento es requerida</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Crear</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        <Link to="/todos" className="btn btn-link">Cancelar</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
    create: todoActions.create

}

const connectedTodoPage = connect(mapState, actionCreators)(TodoPage);
export { connectedTodoPage as TodoPage };