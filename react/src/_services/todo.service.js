import config from 'config';
import { authHeader } from '../_helpers';

export const todoService = {
    getAll,
    create,
    getAll,
    update,
    delete: _delete
};

const requestOptions = {
    method: 'GET',
};

function getAll() {
    let user = localStorage.getItem('userId');
    return fetch(`${config.apiUrl}/todos/todoAll/${JSON.parse(user)}`, requestOptions).then(handleResponse)
}


function create(todo) {
    let userId = JSON.parse(localStorage.getItem('userId'));
    let newTodo = {...todo, userId}
    console.log(newTodo)
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(newTodo)
    };

    return fetch(`${config.apiUrl}/todos/`, requestOptions).then(handleResponse);
}

function update(todo) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(todo)
    };
    console.log(todo)

    return fetch(`${config.apiUrl}/todos`, requestOptions).then(handleResponse);;
}

function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/todos/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}