import { todoConstants } from '../_constants';
import { todoService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const todoActions = {
    create,
    getAll,
    delete: _delete
};

function create(todo, isRegister) {
    return dispatch => {
        dispatch(request(todo));
      if (isRegister) { 
          console.log(todo)
          todoService.create(todo)
            .then(
                todo => { 
                    dispatch(success(todo));
                    dispatch(alertActions.success('Registro exitoso'));
                    history.push('/todos');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
        } else{
            todoService.update(todo)
            .then(
                todo => { 
                    dispatch(success());
                    history.push('/todos');
                    dispatch(alertActions.success('Actualización exitosa'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
        }
    };

    function request(todo) { return { type: todoConstants.CREATE_REQUEST, todo } }
    function success(todo) { return { type: todoConstants.CREATE_SUCCESS, todo } }
    function failure(error) { return { type: todoConstants.CREATE_FAILURE, error } }
}

function getAll() {
    const convertDate = (fecha, offset) =>{
        var targetTime = new Date(fecha);
        var tzDifference = offset + targetTime.getTimezoneOffset();
        //convert the offset to milliseconds, add to targetTime, and make a new Date
        var offsetTime = new Date(targetTime.getTime() + tzDifference * 60 * 1000);
        return offsetTime;
    }
    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const todoA = a.priority;
        const todoB = b.priority;
      
        let comparison = 0;
        if (todoA > todoB) {
          comparison = 1;
        } else if (todoA < todoB) {
          comparison = -1;
        }
        return comparison*-1;
      }
      
    return dispatch => {
        dispatch(request());

        todoService.getAll()
            .then(
                todos => dispatch(success(todos)),
                error => dispatch(failure(error.toString()))
        )
        .then(todos=>todos.todos.sort(compare).map(
            todo=> {
                let fecha = convertDate(todo.expire_at, parseInt(todo.offset));
                let diff= (new Date()-fecha)/3600000;
                diff>0 && diff<24?
               dispatch(alertActions.success('Pŕoxima tarea a vencer: ' + todo.name)):null;
             }
            )
        );
    };

    function request() { return { type: todoConstants.GETALL_REQUEST } }
    function success(todos) { return { type: todoConstants.GETALL_SUCCESS, todos } }
    function failure(error) { return { type: todoConstants.GETALL_FAILURE, error } }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        todoService.delete(id)
            .then(
                todo => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: todoConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: todoConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: todoConstants.DELETE_FAILURE, id, error } }
}