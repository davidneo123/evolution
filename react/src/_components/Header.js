import { Link } from 'react-router-dom';
import React from 'react';

export const Header = props =>
    <div className="row">
    <h2>TAREAS APP</h2>
        {props.user?
            <div className='col-sm-6'><button><Link to="/login">Logout  </Link></button><strong>Welcome {props.user.user.username}!</strong></div>:null}
        {props.user ? //&& props.user.admin
            <div className='col-sm-6'><button><Link to="/users"> Manage Users</Link></button>   </div>:null}
    </div>
