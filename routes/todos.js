const express = require('express');
const router = express.Router();
const Todo = require('../models/todo');

router.post('/', (req, res, next) => {
  var todo = new Todo(req.body);
  todo.save(function(err,resource){
    if(err){
     return res.send(err).status(501);
    } else{
      res.json(resource).status(201);
    }
    });
});

router.get('/',function(req,res){
 Todo.find({},function(err,resource){
   if(err){
     res.send(err).status(404);
   } else {
     res.send(resource).status(200);
   }
 });
});

router.get('/todoAll/:userId',function(req,res){
   var userId= req.params.userId;
  Todo.find({userId},function(err,resource){
    if(err){
      res.send(err).status(404);
    } else {
      res.send(resource).status(200);
    }
  });
});

router.delete('/:id', (req, res,next) => {
  var id = req.params.id;
  Todo.remove({_id:id}, function(err,resource){
    if(err){
     return res.send(err).status(501);
    } else{
      res.send(resource).status(201);
    }
  });
});

router.get('/:id', function(req,res) {
  var id=req.params.id;
  Todo.findById(id,function(err,resource){
    if(err){
     return res.send(err).status(501);
    } else{
      res.json(resource).status(200);
    }
    });
});

router.put('/', function(req,res,next) {
  var todo = new Todo(req.body);
  var id=todo._id;
  todo.updated_at=new Date()
  Todo.updateOne({_id:id},todo, function(err,resource){
    if(err){
     return res.send(err).status(501);
    } else{
      res.json(resource).status(201);
    }
    });
});

module.exports = router;