const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;
const TodoSchema = new Schema({
  name: {
    type: String
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
        // required:true
  },
  priority: {
    type: Number
  },
  expire_at: {
    type: Date
  },
  created_at: {
    type: Date
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  offset: {
      type: String
  }
});
TodoSchema.pre('save', function(next){
  now = new Date()
  same=this;
  //same.updated_at = now
  if ( !same.created_at ) {
    same.created_at = now;
    same.offset= now.getTimezoneOffset();
 }
  next();
 }); 

const Todo = module.exports = mongoose.model('Todo', TodoSchema);